/**
 * Copyright 2014 Aditya Gaddam <www.lostinspacebar.com>
 *
 */
package com.lostinspacebar.maven.plugins.autobuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.maven.model.Build;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;
import org.apache.maven.model.Parent;
import org.apache.maven.model.Plugin;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.model.io.xpp3.MavenXpp3Writer;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

/**
 * The Auto-Builder plugin uses some user provided information about the local project directory structure to scan for
 * changes when a project is built to see if any dependencies also available locally need to be rebuilt before this
 * project is rebuilt.
 *
 * @goal autobuild
 * @phase validate
 * @author Aditya Gaddam <www.lostinspacebar.com>
 */
public class AutoBuilderMojo extends AbstractMojo
{

    /**
     * Current project
     *
     * @parameter default-value="${project}"
     * @required
     * @readonly
     */
    private MavenProject buildProject;

    /**
     * Semi-colon delimited list of directories to check for POM files.
     *
     * @parameter
     */
    private String directories;

    /**
     * Package filter used to reduce POMs we track for changes. Currently unused (aka, everything is scanned). TODO: Use
     * this to filter down what artifacts start off another findChangedProjects recursion.
     *
     * @parameter
     */
    private String packageFilter;

    /**
     * Map from [coordinate] -> LocalProject
     */
    private final Map<String, LocalProject> localProjects = new HashMap<>();

    /**
     * Perform a scan of all dependencies and parents to see if anything needs to be rebuilt before this project is
     * rebuilt.
     *
     * @throws MojoExecutionException
     */
    @Override
    public void execute() throws MojoExecutionException
    {
        System.out.println("------- AUTOBUILDER -------");
        System.out.println("Current Project: " + buildProject.getName() + ":" + buildProject.getArtifactId());

        if (directories == null || directories.isEmpty())
        {
            System.out.println("No directories specified. Autobuilder will do nothing.");
            return;
        }

        String[] monitoredDirectories = directories.split(";");

        System.out.println("Monitoring Directories:");
        for (String dir : monitoredDirectories)
        {
            System.out.println("\t- " + dir);
            Collection<File> files = FileUtils.listFiles(new File(dir), new WildcardFileFilter("pom.xml"), DirectoryFileFilter.DIRECTORY);
            for (File f : files)
            {
                try
                {
                    MavenXpp3Reader reader = new MavenXpp3Reader();
                    Model model = reader.read(new FileReader(f.getAbsolutePath()));
                    String coords = getCoordinates(model);

                    localProjects.put(coords, new LocalProject(coords, model, f.getParentFile()));
                }
                catch (IOException | XmlPullParserException ex)
                {
                    ex.printStackTrace();
                }
            }
        }

        Set<LocalProject> changedProjects = new HashSet<>();
        findChangedProjects(getProject(getCoordinates(buildProject)), changedProjects, -1);

        if (changedProjects.isEmpty())
        {
            System.out.println("No changed projects. Doing nothing.");
            return;
        }

        // Make a temporary pom of the changed projects
        Model autoBuildPom = new Model();
        autoBuildPom.setPackaging("pom");
        autoBuildPom.setGroupId(buildProject.getGroupId());
        autoBuildPom.setArtifactId(buildProject.getArtifactId() + "-autobuider");
        autoBuildPom.setVersion("1.0.0");

        // Deploy Plugin configuration
        // We don't want the autobuilder to end up in whatever intranet or local repository the user is using.
        Plugin deployPlugin = new Plugin();
        deployPlugin.setArtifactId("maven-deploy-plugin");
        deployPlugin.setVersion("2.8.2");
        Xpp3Dom deployPluginConfiguration = new Xpp3Dom("configuration");
        Xpp3Dom skipDom = new Xpp3Dom("skip");
        skipDom.setValue("true");
        deployPluginConfiguration.addChild(skipDom);
        deployPlugin.setConfiguration(deployPluginConfiguration);

        // Setup build DOM with the deploy plugin
        autoBuildPom.setBuild(new Build());
        autoBuildPom.getBuild().getPlugins().add(deployPlugin);
        autoBuildPom.setModelVersion("4.0.0");

        File autoBuildPomFile = null;
        try
        {
            String currentProjectDirectoryPath = buildProject.getModel().getPomFile().getParentFile().getAbsolutePath();

            // Create the autobuilder project in the target folder so we don't risk it being left behind for some 
            // reason and polluting the SVN diffs
            MavenXpp3Writer writer = new MavenXpp3Writer();
            autoBuildPomFile = new File(currentProjectDirectoryPath + File.separator + "target" + File.separator
                    + UUID.randomUUID().toString() + ".xml");
            autoBuildPomFile.deleteOnExit();

            // Add each changed project to the auto builder aggregator POM file
            Path autoBuildPomDirectory = Paths.get(autoBuildPomFile.getParentFile().getAbsolutePath());
            System.out.println("Auto Build Directory: " + autoBuildPomDirectory.toString());
            System.out.println("Changed Dependencies:");
            for (LocalProject changedProject : changedProjects)
            {
                // Aggregator POMs apparently only like relative paths to modules, so we need to relativize everything.
                // This currently limits projects to be on the same drive, but whatever. 
                System.out.println("\t- " + changedProject.getDirectory().getAbsolutePath());
                String relativePath = autoBuildPomDirectory.relativize(Paths.get(changedProject.getDirectory().getAbsolutePath())).toString();
                autoBuildPom.getModules().add(relativePath);
            }

            System.out.println("Creating pom file: " + autoBuildPomFile.getAbsolutePath());
            try (FileOutputStream pomStream = new FileOutputStream(autoBuildPomFile))
            {
                writer.write(pomStream, autoBuildPom);
                pomStream.flush();
            }

            /*
             * System.out.println("Clean + Building all changed dependencies.");
             * InvocationRequest request = new DefaultInvocationRequest();
             * request.setGoals(Arrays.asList("clean", "install"));
             * request.setPomFile(autoBuildPomFile);
             *
             * Invoker invoker = new DefaultInvoker();
             * InvocationResult result = invoker.execute(request);
             * if (result.getExitCode() != 0) {
             * throw new RuntimeException("DEPENDENCY BUILD FAILURE. See above for build results.");
             * }
             */
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new RuntimeException("AUTOBUILDER PLUGIN FAILURE. See above for exception details.");
        }
        finally
        {
            if (autoBuildPomFile != null)
            {
                autoBuildPomFile.delete();
            }
        }
    }

    /**
     * Look through dependencies and the parents (direct and transitive) to see if any files have changed to trigger a
     * project rebuild.
     *
     * @param project         Project to use as a starting point to search for changed dependencies / parents
     * @param changedProjects Set of changed projects that will be added to in the recursive search for changed files.
     * @param targetTime      Target time to check against. This is used to override the target time calculation for a
     *                        project. Also useful if the project doesn't have target files (like parent pom projects).
     *
     * @return true if anything has changed in the dependencies or parents of the specified model.
     */
    private boolean findChangedProjects(LocalProject project, Set<LocalProject> changedProjects, long targetTime)
    {
        if (changedProjects.contains(project))
        {
            return true;
        }

        File targetDirectory = new File(project.getDirectory().getAbsolutePath(), "target");

        boolean changed = false;
        long oldestTargetModifiedTime = -1;

        // Only non-pom projects have target directories (as far as we know)
        if (!project.getModel().getPackaging().equals("pom"))
        {
            if (targetDirectory.exists())
            {
                // Find target files (outputs of the project). If these are newer than all the files, then we are good.
                Collection<File> targetFiles = FileUtils.listFiles(targetDirectory, new WildcardFileFilter("*." + project.getModel().getPackaging()), DirectoryFileFilter.DIRECTORY);

                for (File targetFile : targetFiles)
                {
                    if (oldestTargetModifiedTime == -1 || oldestTargetModifiedTime < targetFile.lastModified())
                    {
                        oldestTargetModifiedTime = targetFile.lastModified();
                    }
                }
            }
            else
            {
                // Hasn't been built yet, add to our changed tree
                changed = true;
            }
        }

        // If targetTime is specified, use that instead
        if (targetTime != -1)
        {
            oldestTargetModifiedTime = targetTime;
        }

        // Check the project's files
        Collection<File> projectFiles = FileUtils.listFiles(project.getDirectory(), TrueFileFilter.TRUE, DirectoryFileFilter.DIRECTORY);
        for (File f : projectFiles)
        {
            if (f.lastModified() > oldestTargetModifiedTime)
            {
                changed = true;
            }
        }

        // Check dependencies
        List<Dependency> deps = project.getModel().getDependencies();
        for (Dependency d : deps)
        {
            LocalProject depProject = getProject(getCoordinates(d));
            if (depProject != null)
            {
                changed |= findChangedProjects(depProject, changedProjects, -1);
            }
        }

        // Check parent
        LocalProject parentProject = getProject(getCoordinates(project.getModel().getParent()));
        if (parentProject != null)
        {
            changed |= findChangedProjects(parentProject, changedProjects, oldestTargetModifiedTime);
        }

        // If something in the local tree has changed AND this isn't the project that started off this maven plugin, 
        // we should add it to the changed projects list. We don't need to build the start-up project since it's being
        // clean and built anyway.
        if (changed && !getCoordinates(project.getModel()).equals(getCoordinates(buildProject)))
        {
            changedProjects.add(project);
        }

        // All done
        return changed;
    }

    private LocalProject getProject(Model model)
    {
        return localProjects.get(getCoordinates(model));
    }

    private LocalProject getProject(String coordinates)
    {
        return localProjects.get(coordinates);
    }

    private String getCoordinates(Parent parent)
    {
        return parent.getGroupId() + ":" + parent.getArtifactId();
    }

    private String getCoordinates(Dependency dependency)
    {
        String groupId = dependency.getGroupId();
        String artifactId = dependency.getArtifactId();

        return groupId + ":" + artifactId;
    }

    private String getCoordinates(Model model)
    {
        String groupId = model.getGroupId() == null ? model.getParent().getGroupId() : model.getGroupId();
        String artifactId = model.getArtifactId() == null ? "" : model.getArtifactId();

        return groupId + ":" + artifactId;
    }

    private String getCoordinates(MavenProject project)
    {
        String groupId = project.getGroupId() == null ? project.getParent().getGroupId() : project.getGroupId();
        String artifactId = project.getArtifactId() == null ? "" : project.getArtifactId();

        return groupId + ":" + artifactId;

    }

    private class LocalProject
    {

        private final String coordinates;
        private final Model model;
        private final File directory;

        public LocalProject(String coordinates, Model model, File directory)
        {
            this.coordinates = coordinates;
            this.model = model;
            this.directory = directory;
        }

        public String getCoordinates()
        {
            return coordinates;
        }

        public Model getModel()
        {
            return model;
        }

        public File getDirectory()
        {
            return directory;
        }

    }

}
